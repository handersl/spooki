//Add in the login screen and don't allow the game to start until a name
//and password is given.
var nameBox = document.getElementsByName("username")[0].value;
var passBox = document.getElementsByName("pass")[0].value;
var loggedIn = false;

//Set up "constants".
var SCREEN_WIDTH = 800;
var SCREEN_HEIGHT = 600;
var ENTITY_BORDER_OFFSET = 2;
var ENTITY_BORDER_DIM = 4;
var PELLET_DIM = 10;
var PELLET_MAX = 4;

//Grab the canvas from the html doc.
var canvas = document.createElement("canvas");
var context = canvas.getContext("2d");
canvas.width = SCREEN_WIDTH + 1000;
canvas.height = SCREEN_HEIGHT;
document.body.appendChild(canvas);

//Set up the variables for the game.
//The time since the last update was called.  Will take care of the fixed time rate.
var timeSinceUpdate = 0;
//The povars of the pellets in game.
var pellets = [];
//The player specs.
var thisPlayerNumber;
var numberOfPlayers;
var playerNames = [];
var playerPoints = [];
var playerPositions = [];
var playerVelocities =[];
var PLAYER_MAX_SPEED = 100;
var playerSpeedNows = [];
var PLAYER_START_DIM = 15;
var playerDims = [];
var PLAYER_SPAWN_BOR = 100;
var scoreboard = [];

//Is the game still running.
var running = false;

//Set the font.
context.font = "30px Garamond";

var frameCount = 0;
var frames = 0;

var timeSinceLastUpdate;

//Delete the log in fields.
function removeSignIn()
{
	var form = document.getElementsByName("theForm")[0];
	form.remove();
}

function checkSignIn()
{
	nameBox = document.getElementsByName("username")[0].value;
	passBox = document.getElementsByName("pass")[0].value;
	
	console.log("Checking login.");
	
	var hash = CryptoJS.MD5(passBox);
	
	socket.emit('login', {'user': nameBox, 'pass': hash.toString()});
}

//Load the game.
function load(data)
{
	if(!running)
	{
		thisPlayerNumber = data.Number;
		numberOfPlayers = data.Amount;
		playerNames = data.PlayerNames;
		playerPoints = data.PlayerPoints;
		playerPositions = data.PlayerPositions;
		playerVelocities = data.PlayerVelocities;
		playerSpeedNows = data.PlayerSpeeds;
		playerDims = data.PlayerDims;
		pellets = data.Pellets;		
		loggedIn = true;
	}
}

function addPlayer(data)
{
	numberOfPlayers = data.Amount;
	playerNames.push(data.PlayerName);
	playerPoints.push(data.PlayerPoint);
	playerPositions.push(data.PlayerPosition);
	playerVelocities.push(data.PlayerVelocitiy);
	playerSpeedNows.push(data.PlayerSpeed);
	playerDims.push(data.PlayerDim);
}

//Start the game.
function start()
{
	if(loggedIn)
	{
		removeSignIn();

		running = true;
		
		var date = new Date();
		timeSinceLastUpdate = date.getTime();
	}
}

//The basic update loop.
function update()
{
	var date = new Date();
	var elapsed = date.getTime() - timeSinceLastUpdate;
	timeSinceLastUpdate = date.getTime();
	
    //Move the players.
    for(var playerNumber = 0; playerNumber < numberOfPlayers; playerNumber++)
	{
		playerPositions[playerNumber] = {x:playerPositions[playerNumber].x + playerVelocities[playerNumber].x*(elapsed/1000), y:playerPositions[playerNumber].y + playerVelocities[playerNumber].y*(elapsed/1000)};
	}
	
	updateScores();
}

//The draw loop of the game.
function draw()
{
    //Clear
    context.clearRect(0, 0, SCREEN_WIDTH + 1000, SCREEN_HEIGHT); 
    
    //Draw the background
    context.fillStyle = "#990099";
    context.fillRect(0,0, SCREEN_WIDTH, SCREEN_HEIGHT);
    
    //Draw all the pellets.
    var len = pellets.length;
    for(var i = 0; i < len; i++)
    {
		//Black borders.
        context.fillStyle = "#000000";
        context.fillRect(pellets[i].x - ENTITY_BORDER_OFFSET, pellets[i].y - ENTITY_BORDER_OFFSET, PELLET_DIM + ENTITY_BORDER_DIM, PELLET_DIM + ENTITY_BORDER_DIM);
		
		//Yellow pellet.
		context.fillStyle = "#F7FE2E";
		context.fillRect(pellets[i].x, pellets[i].y, PELLET_DIM, PELLET_DIM);
    }    
    
    //Draw the players.
	for(var playerNumber = 0; playerNumber < numberOfPlayers; playerNumber++)
	{
		//Black borders.
		context.fillStyle = "#000000";
		context.fillRect(playerPositions[playerNumber].x - ENTITY_BORDER_OFFSET, playerPositions[playerNumber].y - ENTITY_BORDER_OFFSET, playerDims[playerNumber] + ENTITY_BORDER_DIM, playerDims[playerNumber] + ENTITY_BORDER_DIM);
		//Red player.
		if(playerPositions[playerNumber].x == playerPositions[thisPlayerNumber].x && playerPositions[playerNumber].y == playerPositions[thisPlayerNumber].y)
		{
			context.fillStyle = "#66FFCC";
			context.fillRect(playerPositions[playerNumber].x, playerPositions[playerNumber].y, playerDims[playerNumber], playerDims[playerNumber]);
		}
		else
		{
			if(playerDims[thisPlayerNumber] >= playerDims[playerNumber])
			{
				context.fillStyle = "#33FF33";
				context.fillRect(playerPositions[playerNumber].x, playerPositions[playerNumber].y, playerDims[playerNumber], playerDims[playerNumber]);
			}
			else
			{
				context.fillStyle = "#FF6600";
				context.fillRect(playerPositions[playerNumber].x, playerPositions[playerNumber].y, playerDims[playerNumber], playerDims[playerNumber]);
			}
		}
		//Player size.
		context.fillStyle = "black";
		context.font="14px Georgia";
		context.fillText(Math.floor(playerDims[playerNumber]), playerPositions[playerNumber].x + playerDims[playerNumber]/2 - 7, playerPositions[playerNumber].y + playerDims[playerNumber]/2 + 2);
		
		//Print the name on top of the player.
		context.fillStyle = "black";
		context.font="14px Georgia";
		context.fillText(playerNames[playerNumber] + " [" + playerPoints[playerNumber] + "]", playerPositions[playerNumber].x - ENTITY_BORDER_OFFSET, playerPositions[playerNumber].y - 7);
		
		//Print the name in the corner.
		context.fillStyle = "black";
		context.font="25px Georgia";
		context.fillText(scoreboard[playerNumber].name + " " + scoreboard[playerNumber].score, SCREEN_WIDTH, 25 + (playerNumber+1)*25);
	}
	
	//Print the name in the corner.
	context.fillStyle = "black";
	context.font="25px Georgia";
	
	//Scoreboard 
	context.fillText("Scoreboard", SCREEN_WIDTH, 25);
	
	context.fillText(nameBox, 0, SCREEN_HEIGHT - 5);
}

function changeOpVelocity(data)
{
	playerVelocities[data.Player] = data.Velocity;
	playerPositions[data.Player] = data.Position;
}

function disconnect(data)
{
	playerNames.splice(data.Player, 1);
	playerPoints.splice(data.Player,1);
	playerPositions.splice(data.Player, 1);
	playerVelocities.splice(data.Player, 1);
	playerDims.splice(data.Player, 1);
	playerSpeedNows.splice(data.Player, 1);
	numberOfPlayers--;
	
	if(data.Player < thisPlayerNumber)
	{
		thisPlayerNumber--;
	}
}

function setPlayerVelocity(number, newSpeed)
{
	if(playerVelocities[number].x != 0)
	{
		if(playerVelocities[number].x > 0)
		{
			playerVelocities[number] = {x:newSpeed, y:0};
		}
		else
		{
			playerVelocities[number] = {x:-newSpeed, y:0};
		}
	}
	else
	{
		if(playerVelocities[number].y > 0)
		{
			playerVelocities[number] = {x:0, y:newSpeed};
		}
		else
		{
			playerVelocities[number] = {x:0, y:-newSpeed};
		}
	}
}

function pelletEaten(data)
{
	playerDims[data.Eater] = data.Size;
	playerVelocities[data.Eater] = data.Velocity;
	playerSpeedNows[data.Eater] = data.Speed;
	playerPoints[data.Eater] = data.Point;
	pellets.splice(data.Eaten, 1);
	pellets.push({x:data.Pellet.x, y:data.Pellet.y});
}

function playerCrash(data)
{
	resetPlayer(data.Crasher, data.Position);
}

function resetPlayer(crasher, position)
{
	playerPositions[crasher] = {x:position.x, y:position.y};
	playerDims[crasher] = PLAYER_START_DIM;
	//To correct speed.
	if(playerVelocities[crasher].x != 0)
	{
		if(playerVelocities[crasher].x > 0)
		{
			playerVelocities[crasher] = {x: PLAYER_MAX_SPEED, y:0};
		}
		else
		{
			playerVelocities[crasher] = {x: -PLAYER_MAX_SPEED, y:0};
		}
	}
	else if(playerVelocities[crasher].y != 0)
	{
		if(playerVelocities[crasher] > 0)
		{
			playerVelocities[crasher] = {x:0, y:PLAYER_MAX_SPEED};
		}
		else
		{
			playerVelocities[crasher] = {x:0, y:-PLAYER_MAX_SPEED};
		}
	}
}

function playerEat(data)
{
	playerDims[data.Eater] = data.Size;
	playerVelocities[data.Eater] = data.Velocity;
	playerSpeedNows[data.Eater] = data.Speed;
	playerPoints[data.Eater] = data.Point;
	
	resetPlayer(data.Eaten, data.Position);
}

function updateScores()
{
	scoreboard = [];
	for(var i = 0; i < numberOfPlayers; i++)
	{
		scoreboard.push({name:playerNames[i],score:playerPoints[i]});
	}
	
	scoreboard.sort(function(a, b){return b.score-a.score});
}

//Set the frame rate and call the appropriate functions.
var FPS = 60;
setInterval(function() {
if(running)
{
	update();
	draw();
}
}, 1000/FPS);

//Key functions.
document.onkeydown=function(e)
{
    e=window.event || e;
    if(e.keyCode == '38')
    {
        socket.emit('move', {'PlayerNumber': thisPlayerNumber, 'Direction': 'UP'});
    }
    if(e.keyCode == '40')
    {
        socket.emit('move', {'PlayerNumber': thisPlayerNumber, 'Direction': 'DOWN'});
    }
    if(e.keyCode == '37')
    {
        socket.emit('move', {'PlayerNumber': thisPlayerNumber, 'Direction': 'LEFT'});
    }
    if(e.keyCode == '39')
    {
        socket.emit('move', {'PlayerNumber': thisPlayerNumber, 'Direction': 'RIGHT'});
    }
};