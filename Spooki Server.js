//----------------------Below are database functions.-------------------\\
//Setup the database.
//Check to see if the file exsists.
var fs = require("fs");
var spookiDB = "SpookiDB.db";
var exists = fs.existsSync(spookiDB);
//If the file exsists then it will be opened.  If not then
//the file will be created.
var sqlite3 = require("sqlite3").verbose();
var db = new sqlite3.Database(spookiDB);
//Create the file if it does not exsist.
if(!exists)
{
	db.serialize(function()
	{
		//Create a table if it does not exsist.
		//In this case create a table called Login that takes in text.
		db.run("CREATE TABLE Login (user TEXT, pass TEXT)");
	});
}

//Add a new entry in the database.
function addToDatabase(user, pass)
{
	db.serialize(function()
	{
		var stmt = db.prepare("INSERT INTO Login VALUES (?,?)");
		stmt.run(user, pass);
		stmt.finalize();
	});
}

//Look for a specific user.
function lookFor(newUser, newPass, socket)
{
	db.serialize(function()
	{
		var stmt = db.prepare("SELECT user FROM Login WHERE user = (?)");
		var entry = {};
		
		stmt.get(newUser, function(err, row) 
		{
			entry = row;
			
			console.log(entry);
		
			if(entry === undefined)
			{
				console.log('New user.  Starting game.');
				addToDatabase(newUser, newPass);
				var number = addNewPlayer(newUser);
				//Send the game state.
				//Number is an int.  The rest are lists of the game at this time.
				//Sends to new user.
				socket.emit('start', { 'Number': number, 'Amount':numberOfPlayers, 'PlayerNames': playerNames, 'PlayerPoints': playerPoints, 'PlayerPositions':playerPositions, 'PlayerVelocities': playerVelocities, 'PlayerSpeeds': playerSpeedNows, 'PlayerDims': playerDims, 'Pellets': pellets});
				
				//Send to eveyone else.
				socket.broadcast.emit('join', {'Amount':numberOfPlayers, 'PlayerPosition':playerPositions[number], 'PlayerName': playerNames[number], 'PlayerPoint': playerPoint, 'PlayerVelocity': playerVelocities[number], 'PlayerSpeed': playerSpeedNows[number], 'PlayerDim': playerDims[number]});
				
				playerSockets.push(socket);
			}
			else
			{
				console.log('Old user.  Looking for pass pair.');
				lookForPair(newUser, newPass, socket);
			}
		
		});
	});
}

//See if the password matches for a user.
function lookForPair(newUser, newPass, socket)
{
	db.serialize(function()
	{
		var stmt = db.prepare("SELECT user, pass FROM Login WHERE user = (?) AND pass= (?)");
		var entry = {};
		
		stmt.get(newUser, newPass, function(err, row) 
		{
			entry = row;
			
			console.log(entry);
			
			if(entry === undefined)
			{
				console.log('Login does not match.');
				socket.emit('wrongLogin', { 'message': ''});
			}
			else
			{
				console.log('Login match.  Starting game.');
				var number = addNewPlayer(newUser);
				//Sends to new user.
				socket.emit('start', { 'Number': number, 'Amount':numberOfPlayers, 'PlayerNames': playerNames, 'PlayerPoints': playerPoints, 'PlayerPositions':playerPositions, 'PlayerVelocities': playerVelocities, 'PlayerSpeeds': playerSpeedNows, 'PlayerDims': playerDims, 'Pellets': pellets});
				
				//Send to eveyone else.
				socket.broadcast.emit('join', {'Amount':numberOfPlayers, 'PlayerPosition':playerPositions[number], 'PlayerName': playerNames[number], 'PlayerPoint': playerPoints[number], 'PlayerVelocity': playerVelocities[number], 'PlayerSpeed': playerSpeedNows[number], 'PlayerDim': playerDims[number]});
				
				playerSockets.push(socket);
			}
		});
	});
}

//----------------------Below are server functions.-------------------\\
//Create thse server.
var http = require('http'),
fs = require('fs'),
express = require('express');
var appE = express();

//Send index.html to all requests.
app = http.createServer(appE);
	
//Socket.io server listens to our app.
var io = require('socket.io').listen(app);

appE.use(express.static(__dirname + '/'));

//Emit welcome message on connection.  Add in our own handlers for events.
io.sockets.on('connection', function(socket) 
{
	socket.emit('welcome', { 'message': ''});
	
	socket.on('login', function(soc)
	{
		console.log('Looking for: ' + soc.user + ' ' + soc.pass);
		lookFor(soc.user, soc.pass, socket);
	});
	
	socket.on('move', function(soc)
	{
		//Change the player velocity.
		if(soc.Direction == 'UP')
		{
			playerVelocities[soc.PlayerNumber] = {x:0, y:-playerSpeedNows[soc.PlayerNumber]};
		}
		if(soc.Direction == 'DOWN')
		{
			playerVelocities[soc.PlayerNumber] = {x:0, y:playerSpeedNows[soc.PlayerNumber]};
		}
		if(soc.Direction == 'LEFT')
		{
			playerVelocities[soc.PlayerNumber] = {x:-playerSpeedNows[soc.PlayerNumber], y:0};
		}
		if(soc.Direction == 'RIGHT')
		{
			playerVelocities[soc.PlayerNumber] = {x:playerSpeedNows[soc.PlayerNumber], y:0};
		}
		//Tell all the other players but the original.
		//soc.Player is an int.  Velocity is a list pair with x velocity and y.
		io.sockets.emit('move', {'Player': soc.PlayerNumber, 'Position': playerPositions[soc.PlayerNumber], 'Velocity' : playerVelocities[soc.PlayerNumber]});
	});
	
	socket.on('disconnect', function(soc)
	{
		//Find out which player left.
		var playerDis = playerSockets.indexOf(socket);
		
		if(playerDis >= 0)
		{
			playerSockets.splice(playerDis, 1);
			
			console.log('Disconnecting Player: ' + playerDis);
			
			//Delete the player.
			playerNames.splice(playerDis, 1);
			playerPoints.splice(playerDis, 1);
			playerPositions.splice(playerDis, 1);
			playerVelocities.splice(playerDis, 1);
			playerDims.splice(playerDis, 1);
			playerSpeedNows.splice(playerDis, 1);
			numberOfPlayers--;
			
			//Soc.Player is an int.
			io.sockets.emit('disconnect', {'Player': playerDis});
		}
	});
});

//Game Logic.

//Set up "constants".
var SCREEN_WIDTH = 800;
var SCREEN_HEIGHT = 600;
var ENTITY_BORDER_OFFSET = 2;
var ENTITY_BORDER_DIM = 4;
var PELLET_DIM = 10;
var PELLET_MAX = 4;

//Set up the variables for the game.
//The time since the last update was called.  Will take care of the fixed time rate.
var timeSinceUpdate = 0;
//The points of the pellets in game.
var pellets = [];
//The players specs.
var numberOfPlayers = 0;
//The sockets.
var playerSockets = [];
var playerNames = [];
var playerPoints = [];
var playerPositions = [];
var playerVelocities = [];
var playerSpeedNows = [];
var PLAYER_MAX_SPEED = 100;
var PLAYER_START_DIM = 15;
var playerDims = [];
var PLAYER_SPAWN_BOR = 100;

//Is the game still running.
var running = false;

var frameCount = 0;
var frames = 0;

var timeSinceLastUpdate;

//Load the game with no players.
function init()
{
	//Spawn the first pellet.
	for(var i = 0; i < PELLET_MAX; i++)
	{
		pellets.push({x:Math.floor((Math.random()*(SCREEN_WIDTH - PELLET_DIM))+1), y:Math.floor((Math.random()*(SCREEN_HEIGHT - PELLET_DIM))+1)});
	}
}

function addNewPlayer(name)
{
	var currentPlayer = numberOfPlayers;
	numberOfPlayers++;
	
	var newPlayerPosX = Math.floor((Math.random()*(SCREEN_WIDTH - PLAYER_SPAWN_BOR))+1);
	var newPlayerPosY = Math.floor((Math.random()*(SCREEN_HEIGHT - PLAYER_SPAWN_BOR))+1);
	
	playerNames.push(name);
	playerPoints.push(0);
	playerPositions.push({x:newPlayerPosX, y:newPlayerPosY});
	playerVelocities.push({x:0, y:PLAYER_MAX_SPEED});
	playerSpeedNows.push(PLAYER_MAX_SPEED);
	playerDims.push(PLAYER_START_DIM);
	
	return currentPlayer;
}

//Start the game.
function start()
{
	running = true;
	var date = new Date();
	timeSinceLastUpdate = date.getTime();
}

//The basic update loop.
function update()
{
	var date = new Date();
	var elapsed = date.getTime() - timeSinceLastUpdate;
	timeSinceLastUpdate = date.getTime();
	
    //Move the players.
	for(var playerNumber = 0; playerNumber < numberOfPlayers; playerNumber++)
	{
		playerPositions[playerNumber] = {x:playerPositions[playerNumber].x + playerVelocities[playerNumber].x*(elapsed/1000), y:playerPositions[playerNumber].y + playerVelocities[playerNumber].y*(elapsed/1000)};
	}
    
	//Check for collisions with pellets and update the state sccordingly.
	for(var i = 0; i < pellets.length; i++)
	{
		for(var playerNumber = 0; playerNumber < numberOfPlayers; playerNumber++)
		{
			var playerRect = {x:playerPositions[playerNumber].x, y:playerPositions[playerNumber].y, w:playerDims[playerNumber], h:playerDims[playerNumber]};
			var pelletRect = {x:pellets[i].x, y:pellets[i].y, w:PELLET_DIM, h:PELLET_DIM};
			if(checkCollision(playerRect, pelletRect))
			{
				console.log("Collision with pellets.");
				//Grow the player.
				playerDims[playerNumber] *= 1.15;
				playerSpeedNows[playerNumber] *= .925;
				
				setPlayerVelocity(playerNumber, playerSpeedNows[playerNumber]);
				
				//Delete the old pellet.
				pellets.splice(i, 1);
				
				//Generate a new pellet.
				var newPelletX = Math.floor((Math.random()*(SCREEN_WIDTH - PELLET_DIM))+1);
				var newPelletY = Math.floor((Math.random()*(SCREEN_HEIGHT - PELLET_DIM))+1); 
				pellets.push({x: newPelletX, y: newPelletY});
				
				//Increase points.
				playerPoints[playerNumber]++;
				
				//Send the number of the player that ate the pellet, the number of
				//the pellet that was eaten and the position of the new pellet.
				io.sockets.emit('pellet', {'Eater': playerNumber, 'Size': playerDims[playerNumber], 'Velocity': playerVelocities[playerNumber], 'Speed': playerSpeedNows[playerNumber], 'Point': playerPoints[playerNumber], 'Eaten': i, 'Pellet' : {x: newPelletX, y: newPelletY}});
			}
		}
	}
	
	//Check for collisions with borders.
	for(var playerNumber = 0; playerNumber < numberOfPlayers; playerNumber++)
	{
		if(playerPositions[playerNumber].x < 0 || playerPositions[playerNumber].y < 0 || playerPositions[playerNumber].x + playerDims[playerNumber] > SCREEN_WIDTH || playerPositions[playerNumber].y + playerDims[playerNumber] > SCREEN_HEIGHT)
		{
			var newPlayerPos = resetPlayer(playerNumber);
			
			//Send the message to everyone.  Client might have to do velocity
			//correction as well.
			io.sockets.emit('wall', {'Crasher': playerNumber, 'Position' : {x: newPlayerPos.x, y: newPlayerPos.y}});
		}
	}
	
	//Check for collisions with players.
	for(var playerNumber1 = 0; playerNumber1 < numberOfPlayers; playerNumber1++)
	{
		var playerRect1 = {x:playerPositions[playerNumber1].x, y:playerPositions[playerNumber1].y, w:playerDims[playerNumber1], h:playerDims[playerNumber1]};
		
		for(var playerNumber2 = 0; playerNumber2 < numberOfPlayers; playerNumber2++)
		{
			var playerRect2 = {x:playerPositions[playerNumber2].x, y:playerPositions[playerNumber2].y, w:playerDims[playerNumber2], h:playerDims[playerNumber2]};
			
			if(playerRect1.x != playerRect2.x && playerRect1.y != playerRect2.y && playerRect1.w != playerRect2.w && playerRect1.h != playerRect2.h)
			{
				if(checkCollision(playerRect1, playerRect2))
				{
					if(playerDims[playerNumber1] > playerDims[playerNumber2])
					{
						//Grow eater size by eaten and slow it down.
						playerDims[playerNumber1] += playerDims[playerNumber2];
						playerSpeedNows[playerNumber1] *= .925;
						
						setPlayerVelocity(playerNumber1, playerSpeedNows[playerNumber1]);
						
						//Reset the eaten.
						var newPlayerPos = resetPlayer(playerNumber2);
						
						//Increase points.
						playerPoints[playerNumber1] += 10;
						
						//Send the message.  The player that ate, the player that was eaten and the new position of the dead player.
						io.sockets.emit('playerEaten', {'Eater': playerNumber1, 'Size': playerDims[playerNumber1], 'Velocity': playerVelocities[playerNumber1], 'Speed':playerSpeedNows[playerNumber1], 'Point': playerPoints[playerNumber1], 'Eaten' : playerNumber2, 'Position' : {x: newPlayerPos.x, y: newPlayerPos.y}});
					}
					else if(playerDims[playerNumber1] < playerDims[playerNumber2])
					{
						//Grow eater size by eaten.
						playerDims[playerNumber2] += playerDims[playerNumber1];
						playerSpeedNows[playerNumber2] *= .925;
						
						setPlayerVelocity(playerNumber2, playerSpeedNows[playerNumber2]);
						
						//Reset the eaten.
						var newPlayerPos = resetPlayer(playerNumber1);
						
						//Increase points.
						playerPoints[playerNumber2] += 10;
						
						//Send the message.
						io.sockets.emit('playerEaten', {'Eater': playerNumber2, 'Size': playerDims[playerNumber2], 'Velocity': playerVelocities[playerNumber2], 'Speed': playerSpeedNows[playerNumber2], 'Point': playerPoints[playerNumber2], 'Eaten' : playerNumber1, 'Position' : {x: newPlayerPos.x, y: newPlayerPos.y}});
					}
				}
			}
		}
	}
}

function setPlayerVelocity(number, newSpeed)
{
	if(playerVelocities[number].x != 0)
	{
		if(playerVelocities[number].x > 0)
		{
			playerVelocities[number] = {x:newSpeed, y:0};
		}
		else
		{
			playerVelocities[number] = {x:-newSpeed, y:0};
		}
	}
	else
	{
		if(playerVelocities[number].y > 0)
		{
			playerVelocities[number] = {x:0, y:newSpeed};
		}
		else
		{
			playerVelocities[number] = {x:0, y:-newSpeed};
		}
	}
}

function resetPlayer(playerNumber)
{
	var newPlayerPosX = Math.floor((Math.random()*(SCREEN_WIDTH - PLAYER_SPAWN_BOR))+1);
	var newPlayerPosY = Math.floor((Math.random()*(SCREEN_HEIGHT - PLAYER_SPAWN_BOR))+1);
	playerPositions[playerNumber] = {x:newPlayerPosX, y:newPlayerPosY};
	playerDims[playerNumber] = PLAYER_START_DIM;
	playerSpeedNows[playerNumber] = PLAYER_MAX_SPEED;
	//To correct speed.
	if(playerVelocities[playerNumber].x != 0)
	{
		if(playerVelocities[playerNumber].x > 0)
		{
			playerVelocities[playerNumber] = {x: PLAYER_MAX_SPEED, y:0};
		}
		else
		{
			playerVelocities[playerNumber] = {x: -PLAYER_MAX_SPEED, y:0};
		}
	}
	else if(playerVelocities[playerNumber].y != 0)
	{
		if(playerVelocities[playerNumber] > 0)
		{
			playerVelocities[playerNumber] = {x:0, y:PLAYER_MAX_SPEED};
		}
		else
		{
			playerVelocities[playerNumber] = {x:0, y:-PLAYER_MAX_SPEED};
		}
	}
	
	return {x: newPlayerPosX, y: newPlayerPosY};
}

function checkCollision(rect1, rect2)
{
	return(rect1.x < rect2.x + rect2.w && rect1.x + rect1.w > rect2.x && rect1.y < rect2.y + rect2.h && rect1.y + rect1.h > rect2.y);
}

//Set the frame rate and call the appropriate functions.
init();
start();
var FPS = 60;
setInterval(function() {
if(running)
{
	update();
}
}, 1000/FPS);

app.listen(3000);